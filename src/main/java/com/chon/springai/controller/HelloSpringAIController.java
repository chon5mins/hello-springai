package com.chon.springai.controller;

import org.springframework.ai.chat.ChatClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.chon.springai.dto.MessageDto;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class HelloSpringAIController {
	private final ChatClient chatClient;

	// private final OllamaChatClient chatClient;

	@PostMapping("/api/hello-spring-ai")
	public MessageDto postHelloSpringAi(@RequestBody MessageDto message) {
        MessageDto res = new MessageDto();
        res.setMessage(chatClient.call(message.getMessage()));
		return res;
	}
}
