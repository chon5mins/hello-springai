package com.chon.springai.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.ai.chat.ChatClient;
import org.springframework.ai.chat.ChatResponse;
import org.springframework.ai.chat.messages.AssistantMessage;
import org.springframework.ai.chat.messages.Message;
import org.springframework.ai.chat.messages.UserMessage;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.chat.prompt.SystemPromptTemplate;
import org.springframework.ai.document.Document;
import org.springframework.ai.ollama.OllamaChatClient;
import org.springframework.ai.vectorstore.VectorStore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.chon.springai.dto.MessageDto;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
public class PromptWithRagController {

    @Value("classpath:/data/bikes.json")
	private Resource bikesResource;

	@Value("classpath:/prompts/qa-system.st")
	private Resource systemBikePrompt;

	private final ChatClient chatClient;

	private final VectorStore vectorStore;

    @PostMapping("/api/prompt-with-rag")
	public AssistantMessage postPromptWithRag(@RequestBody MessageDto message) {
		

        // Step 3 retrieve related documents to query
		log.info("Retrieving relevant documents");
		List<Document> similarDocuments = vectorStore.similaritySearch(message.getMessage());
		log.info(String.format("Found %s relevant documents.", similarDocuments.size()));

        // // Step 4 Embed documents into SystemMessage with the `system-qa.st` prompt
		log.info("Building Message.");
		String documents = similarDocuments.stream().map(item -> item.getContent()).collect(Collectors.joining("\n"));
		SystemPromptTemplate systemPromptTemplate = new SystemPromptTemplate(systemBikePrompt);

		Message systemMessage = systemPromptTemplate.createMessage(Map.of("documents", documents));
		UserMessage userMessage = new UserMessage(message.getMessage());

		// Step 4 - Ask the AI model
		log.info("Asking AI model to reply to question.");
		Prompt prompt = new Prompt(List.of(systemMessage, userMessage));
		log.info(prompt.toString());

		ChatResponse chatResponse = chatClient.call(prompt);
		log.info("AI responded.");

		log.info(chatResponse.getResult().getOutput().getContent());
		return chatResponse.getResult().getOutput();
	}

}
