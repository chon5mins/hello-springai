package com.chon.springai.controller;

import java.util.Map;

import org.springframework.ai.chat.ChatClient;
import org.springframework.ai.chat.messages.AssistantMessage;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.chat.prompt.PromptTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.chon.springai.dto.PromptTemplateDto;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class PromptTemplateController {
    private final ChatClient chatClient;

    @Value("classpath:/prompts/prompt-template.st")
	private Resource promptTempalte;

    @PostMapping("/api/prompt-template")
    public AssistantMessage postPromptTempate(@RequestBody PromptTemplateDto promptTemplateDto){
        PromptTemplate promptTemplate = new PromptTemplate(promptTempalte);
		Prompt prompt = promptTemplate.create(Map.of("lang1", promptTemplateDto.getLang1(), "lang2", promptTemplateDto.getLang2()));

		return chatClient.call(prompt).getResult().getOutput();
    }
    

}
