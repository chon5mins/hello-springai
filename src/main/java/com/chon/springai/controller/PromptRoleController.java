package com.chon.springai.controller;

import java.util.List;
import java.util.Map;

import org.springframework.ai.chat.ChatClient;
import org.springframework.ai.chat.messages.AssistantMessage;
import org.springframework.ai.chat.messages.Message;
import org.springframework.ai.chat.messages.SystemMessage;
import org.springframework.ai.chat.messages.UserMessage;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.chat.prompt.SystemPromptTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.chon.springai.dto.PromptRoleDto;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class PromptRoleController {
    private final ChatClient chatClient;

	// private final OllamaChatClient chatClient;

    @Value("classpath:/prompts/system-message.st")
	private Resource systemResource;

    @PostMapping("/api/prompt-role")
	public AssistantMessage postPromptRole(@RequestBody PromptRoleDto promptRoleDto) {
		SystemPromptTemplate systemPromptTemplate = new SystemPromptTemplate(systemResource);
		Message systemMessage = systemPromptTemplate.createMessage(Map.of("name", promptRoleDto.getAgentName(), "voice", promptRoleDto.getVoiceTone()));

        Message userMessage = new UserMessage(promptRoleDto.getQuestion());

		Prompt prompt = new Prompt(List.of(systemMessage,userMessage));

		return chatClient.call(prompt).getResult().getOutput();
	}
}
