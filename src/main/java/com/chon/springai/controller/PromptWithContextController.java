package com.chon.springai.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.ai.chat.ChatClient;
import org.springframework.ai.chat.messages.AssistantMessage;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.chat.prompt.PromptTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.chon.springai.dto.MessageDto;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class PromptWithContextController {
	private final ChatClient chatClient;

	@Value("classpath:/docs/chon-profile.md")
	private Resource chonProfile;

	@Value("classpath:/prompts/qa-prompt.st")
	private Resource qaPromptResource;


	@PostMapping("/api/prompt-with-context")
	public AssistantMessage postPromptWithContext(@RequestBody MessageDto message) {
		PromptTemplate promptTemplate = new PromptTemplate(qaPromptResource);

		Map<String, Object> map = new HashMap<>();
		map.put("question", message.getMessage());
        map.put("context", chonProfile);

		Prompt prompt = promptTemplate.create(map);

		return chatClient.call(prompt).getResult().getOutput();
	}
}
