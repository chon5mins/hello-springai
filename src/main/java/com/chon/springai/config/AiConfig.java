package com.chon.springai.config;

import org.springframework.ai.embedding.EmbeddingClient;
import org.springframework.ai.reader.JsonReader;
import org.springframework.ai.vectorstore.PgVectorStore;
import org.springframework.ai.vectorstore.SimpleVectorStore;
import org.springframework.ai.vectorstore.VectorStore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class AiConfig {

    @Value("classpath:/data/bikes.json")
	private Resource bikesResource;
    
    @Value("classpath:/data/recipes.json")
	private Resource recipesResource;

    @Bean
    VectorStore vectorStore(EmbeddingClient embeddingClient){
        return new SimpleVectorStore(embeddingClient);
    }

    // @Bean
    // VectorStore vectorStore(JdbcTemplate jdbcTemplate, EmbeddingClient embeddingClient) {
    //     return new PgVectorStore(jdbcTemplate, embeddingClient);
    // }


    @Bean
    ApplicationRunner loadBikes(VectorStore vectorStore){
         // Step 1 - Load JSON document as Documents
         // Step 2 - Create embeddings and save to vector store
        return args -> {
            vectorStore.add(new JsonReader(bikesResource, "name", "price", "shortDescription", "description").get());
            // vectorStore.add(new JsonReader(recipesResource, "Name", "Description", "Author", "Ingredients","Method").get());
        };
    }
}
