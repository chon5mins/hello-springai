package com.chon.springai.dto;

import lombok.Data;

@Data
public class MessageDto {
    private String message;
}
