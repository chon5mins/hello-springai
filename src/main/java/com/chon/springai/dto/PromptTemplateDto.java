package com.chon.springai.dto;

import lombok.Data;

@Data
public class PromptTemplateDto {

    private String lang1;
    private String lang2;

}
