package com.chon.springai.dto;

import lombok.Data;

@Data
public class PromptRoleDto {
    private String agentName;
    private String voiceTone;
    private String question;
}
