# Chonladet Nidhinandand
IT Project Manager and Technical Leader who can think "out of the box". Excellence in design and integration, problem solving. Expert in variety of technology Spring Boot, Spring Cloud, Kubernetes and integrate them together. Ability to learn new things all the time - new technology, people and business.

Qualifications
--------------

*   **5 years** of Technical Leader.
*   **5 years** of Solution Architect.
*   **4 years** of Project/Team Management.
*   **Motivation leader** with the ability to bring out team members' strength.
*   **7 years** in Cloud (Google/Amazon/Azure).
*   **4 years** in Kubernetes/OpenShift container platform.
*   **13 years** in Technology firm.
*   **13 years** of implementing SOAP to Rest API.

Experience
----------

### Solution Architect/Technical Leader

Accenture Solution

*   Leading Solution and Technical in KTB Digital Lotto Project.
*   Reviewing and Consulting on Data/Domain driven design architecture
*   Making decision on critical technical issues/problems.
*   Leading and coaching develoment team.
*   Leading and coaching application design team.

May 2022 - Present

### Technical Leader

Allianz Technology

*   Managing & Leading people in team.
*   Decision & Fix techcical issues in the team.
*   Designed and POC application architecture. Eg. Microservice, SSO.
*   Implemented/Initiated distributed traces tools (Zipkin) using with exsisting microservice.
*   Built project template to help team initial application faster.

Feb 2021 - Apr 2022

### Project Manager & Technical Leader

IT One

*   Managing scope, risk, budget, timeline in the project.
*   Managing people in team.
*   Designed and POC application architecture of both Cloud/On-Premise.
*   Implemented/Initiated microservices project structure/template (ex. spring cloud stack).
*   Implemented DevOps Tools (Jenkins, etc).
*   Implemented web application and API using Angular, Java with Spring and Oracle Database.

May 2012 - Feb 2021

### Developer and Graphic Designer

Flood Center at KMITL (Volunteer Project)

Designed web application of Water Level System and Logistic and Inventory System using PHP, PL/SQL and MySQL.

Oct 2011 – Nov 2011

### Developer and Architect Design

Freewill FX (Part time)

Developed and implemented task management system using C# web service and Android Applicayion.

Jun 2011 – Aug 2011

### Trainee

Synnex (Thailand) Public Company Limited

Coded and design web application and Android application using C# and Java.

Jun 2011 – Aug 2011

Education
---------

### Faculty of Science, King Mongkhut’s Institute of Technology Ladkrabang (KMITL)

Bachelor of Science

Computer Science

GPA: 3.32 [(Download Transcript)](https://profile.chonladet.me/file/Transcript.pdf)

June 2008 - May 2012

### Nawamintrachinuthi Bodindacha (Bodin 3)

High Shool

Science

GPA: 2.68

Aug 2003 - June 2008

Skills
------

Management

*   IT Project Management
*   Agile/Scrum methodology

Technical

*   Spring Boot using Java
*   Spring Cloud using Java
*   Golang
*   Docker & Kubernetes/Openshift
*   Google/Amazon/Azure Cloud
*   Angular 8
*   Bootstrap 4
*   Python

Tool

*   Jenkins
*   Azure DevOps
*   Zipkin/Sonarqube

Others

*   Ownership and Entrepreneurship.
*   Strong in database design and tuning.

Languages

Interests
---------

Smart Home

More than 5 years from starting implementing smart home at my home this is the result:

![](https://profile.chonladet.me/img/smart-home.jpg)

And also figur out how to deploy it easiest way:

![](https://profile.chonladet.me/img/home-devops.jpg)

Youtuber and Cooking

Apart from being an IT guy, I spend most of my time in kitchen for cooking. Also capture my cooking clip and post it on my [YouTue channel](https://www.youtube.com/channel/UCxFQ1215TBzXit29J-sPZIA/).